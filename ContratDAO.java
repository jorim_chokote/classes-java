import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe d'acc�s aux donn�es contenues dans la table contrat
 * 
 * @author Chokote & Leondaris
 * @version 1.0
 * */
public class ContratDAO {

	/**
	 * Paramètres de connexion à la base de données oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "SYSTEM";  //exemple BDD1
	final static String PASS = "jorim1996";   //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public ContratDAO() {
		// chargement du pilote de bases de données
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err
					.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * Permet d'ajouter un client dans la table client Le mode est auto-commit
	 * par défaut : chaque insertion est validée
	 * 
	 * @param client
	 *            le client à ajouter
	 * @return retourne le nombre de lignes ajoutées dans la table
	 */
	public int ajouter(Contrat contrat) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;

		// connexion à la base de données
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// préparation de l'instruction SQL, chaque ? représente une valeur
			// à communiquer dans l'insertion
			// les getters permettent de récupérer les valeurs des attributs
			// souhaités
			
			ps = con.prepareStatement("INSERT INTO contrat_ctr (id_ctr, date_deb_ctr, date_fin_ctr, id_ctr_clt) VALUES (?, ?, ?, ?)");
			ps.setDouble (1, contrat.getIdentifiant());
			ps.setDate(2, contrat.getDateDebut());
			ps.setDate(3, contrat.getDateFin());
			ps.setDouble(4, contrat.getClient().getId());


			// Ex�cution de la requ�te
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de retrouver un contrat � partir d'un client
	 * @param client
	 * @return
	 */
	public Contrat getContrat(Client client) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Contrat retour = null;

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM contrat_ctr  WHERE id_ctr_clt = ?");
			ps.setDouble(1, client.getId());

			// on exécute la requête
			// rs contient un pointeur situé juste avant la première ligne
			// retournée
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			if (rs.next())
				retour = new Contrat(rs.getDouble("id_ctr"),
						rs.getDate("date_deb_ctr"),
						rs.getDate("date_fin_ctr"),
						client);

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer tous les clients stockés dans la table client_clt
	 * 
	 * @return une ArrayList de Client
	 */
	public List<Contrat> getListeContrat() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Contrat> retour = new ArrayList<Contrat>();
		ClientDAO clientDAO = new ClientDAO();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM contrat_ctr INNER JOIN client_clt ON contrat_ctr.id_ctr_clt = client_clt.id_clt ");

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next())
				retour.add(new Contrat(rs.getDouble("id_clt"), rs.getDate("date_deb_ctr"), rs.getDate("date_fin_ctr"), clientDAO.getClient(rs.getLong("siret_clt"))));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	// main permettant de tester la classe
	public static void main(String[] args) throws SQLException {

		ContratDAO contratDAO = new ContratDAO();
		// test de la m�thode ajouter
		Client c1 = new Client(7, "BOSCH", 12365478526572L, "2 Rue Charles" , "0746H");
		//Contrat a1 = new Contrat(3, Date.valueOf("2017-05-08"), Date.valueOf("2017-05-10"), c1 );
		//int retour = contratDAO.ajouter(a1);

		//System.out.println(retour + " lignes ajoutées");

		//test de la m�thode getContrat
		Contrat a2 = contratDAO.getContrat(c1);
		System.out.println(a2);

		// test de la méthode getListeContrat
		List<Contrat> liste = contratDAO.getListeContrat();
		// affichage des clients
		for (Contrat ctr : liste) {
			System.out.println(ctr.toString());
		}

	}
}
