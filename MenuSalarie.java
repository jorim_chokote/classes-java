import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import net.miginfocom.swing.MigLayout;
import javax.swing.SpringLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MenuSalarie extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuSalarie frame = new MenuSalarie();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MenuSalarie() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("BIENVENUE");
		lblNewLabel.setBounds(192, 34, 108, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblQueVoulezVous = new JLabel("Que voulez vous faire ?");
		lblQueVoulezVous.setBounds(158, 59, 156, 14);
		contentPane.add(lblQueVoulezVous);
		
		JButton btnSaisirUnClient = new JButton("Saisir un client");
		btnSaisirUnClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new ClientGUI ();
			}
		});
		btnSaisirUnClient.setBounds(147, 83, 156, 23);
		contentPane.add(btnSaisirUnClient);
		
		JButton btnNewButton = new JButton("Saisir un op\u00E9rateur");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setBounds(147, 117, 156, 23);
		contentPane.add(btnNewButton);
		
		JButton btnSaisirUneMaintenance = new JButton("Saisir une maintenance");
		btnSaisirUneMaintenance.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnSaisirUneMaintenance.setBounds(147, 151, 156, 23);
		contentPane.add(btnSaisirUneMaintenance);
		
		JButton btnModifierUneMaintenance = new JButton("Modifier une maintenance");
		btnModifierUneMaintenance.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnModifierUneMaintenance.setBounds(147, 185, 156, 23);
		contentPane.add(btnModifierUneMaintenance);
		
		JButton btnModifierUnDevis = new JButton("Modifier un devis");
		btnModifierUnDevis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnModifierUnDevis.setBounds(147, 219, 156, 23);
		contentPane.add(btnModifierUnDevis);
	}
}
