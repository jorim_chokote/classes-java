import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.JTextField;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import net.miginfocom.swing.MigLayout;
import javax.swing.BoxLayout;
import java.awt.CardLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.JRadioButtonMenuItem;
import java.awt.event.ActionListener;
import javax.swing.SpringLayout;
import javax.swing.JPasswordField;

public class MenuPrincipal extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldLogin;
    private String login;
    private String mdp;
    private Connexion connexion = new Connexion();
    private JPasswordField textFieldMdp;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuPrincipal frame = new MenuPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MenuPrincipal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("");
		label.setBounds(17, 29, 0, 0);
		contentPane.add(label);
		
		JLabel label_2 = new JLabel("");
		label_2.setBounds(21, 29, 0, 0);
		contentPane.add(label_2);
		
		JLabel label_3 = new JLabel("");
		label_3.setBounds(25, 29, 0, 0);
		contentPane.add(label_3);
		
		JLabel label_1 = new JLabel("");
		label_1.setBounds(128, 29, 0, 0);
		contentPane.add(label_1);
		
		JLabel lblBienvenue = new JLabel("BIENVENUE");
		lblBienvenue.setBounds(197, 45, 71, 14);
		contentPane.add(lblBienvenue);
		
		JLabel lblLogin = new JLabel("LOGIN");
		lblLogin.setBounds(41, 123, 55, 14);
		contentPane.add(lblLogin);
		
		JLabel lblMotDePasse = new JLabel("MOT DE PASSE");
		lblMotDePasse.setBounds(41, 147, 87, 14);
		contentPane.add(lblMotDePasse);
		
		textFieldLogin = new JTextField();
		textFieldLogin.setBounds(207, 120, 117, 20);
		contentPane.add(textFieldLogin);
		textFieldLogin.setColumns(10);
		
		JButton btnConnexion = new JButton("Connexion");
		btnConnexion.setBounds(229, 175, 83, 23);
		contentPane.add(btnConnexion);
		
		textFieldMdp = new JPasswordField();
		textFieldMdp.setBounds(207, 144, 117, 20);
		contentPane.add(textFieldMdp);
		
		btnConnexion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
			 try{
				if (ae.getSource() == btnConnexion){
					login = textFieldLogin.getText();
					mdp = textFieldMdp.getText();
					if (connexion.seConnecter(login, mdp).equals("Salarie")){
						MenuSalarie frame = new MenuSalarie();
						frame.setVisible(true);
					}
					
					else {
						System.out.println("Jorim");
					}
						
						
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(contentPane, "Erreur mot de passe ou login",
						"Erreur", JOptionPane.ERROR_MESSAGE);
				}

					
			}
		});
		
		
		
	}
}
