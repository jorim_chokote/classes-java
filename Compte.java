/**
 * Classe compte
 * @author Chokote & Leondaris
 * @version 1.0
 */
public class Compte {
	
	private double identifiant;
	
	/**
	 * Fonction du compte
	 */
	private String fonction;
	
	/**
	 * Constructeur
	 * @param identifiant
	 * @param fonction
	 */
	public Compte (double identifiant, String fonction) {
		this.identifiant = identifiant;
		this.fonction = fonction;
	}

	public double getIdentifiant(){
		return identifiant;
	}
	
	/**
	 * Getter de la fonction
	 * @return
	 */
	public String getFonction(){
		return fonction;
	}
	
	public void setIdentifiant(double identifiant){
		this.identifiant = identifiant;
	}
	
	/**
	 * Setter de la fonction
	 * @param fonction
	 */
	public void setFonction (String fonction){
		this.fonction = fonction;
	}
	
	public String toString() {
		return "Compte : " + fonction ; 
	}

	
	
}
