import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe d'acc�s aux donn�es contenues dans la table contrat
 * 
 * @author Chokote & Leondaris
 * @version 1.0
 * */
public class CompteDAO {

	/**
	 * Paramètres de connexion à la base de données oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "SYSTEM";  //exemple BDD1
	final static String PASS = "jorim1996";   //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public CompteDAO() {
		// chargement du pilote de bases de données
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err
					.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * Permet d'ajouter un client dans la table client Le mode est auto-commit
	 * par défaut : chaque insertion est validée
	 * 
	 * @param client
	 *            le client à ajouter
	 * @return retourne le nombre de lignes ajoutées dans la table
	 */
	public int ajouter(Compte compte) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;

		// connexion à la base de données
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// préparation de l'instruction SQL, chaque ? représente une valeur
			// à communiquer dans l'insertion
			// les getters permettent de récupérer les valeurs des attributs
			// souhaités
			
			ps = con.prepareStatement("INSERT INTO compte_cpt (id_cpt, fonction_cpt) VALUES (?, ?)");
			ps.setDouble(1, compte.getIdentifiant());
			ps.setString(2, compte.getFonction());


			// Ex�cution de la requ�te
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de retrouver un contrat � partir d'un client
	 * @param client
	 * @return
	 */
	public Compte getCompte(String fonction) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Compte retour = null;

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM compte_cpt WHERE fonction_cpt = ? ");
			ps.setString(1, fonction);

			// on exécute la requête
			// rs contient un pointeur situé juste avant la première ligne
			// retournée
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			if (rs.next())
				retour = new Compte(rs.getDouble("id_cpt"),fonction);

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer tous les clients stockés dans la table client_clt
	 * 
	 * @return une ArrayList de Client
	 */
	public List<Compte> getListeComtpe() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Compte> retour = new ArrayList<Compte>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM compte_cpt ");

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next())
				retour.add(new Compte(rs.getDouble("id_cpt"), rs.getString("fonction_cpt")));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	// main permettant de tester la classe
	public static void main(String[] args) throws SQLException {
		
		CompteDAO compteDAO = new CompteDAO();
		//test de la m�thode ajouter
		Compte compte = new Compte(1, "Salarie");
		int retour = compteDAO.ajouter(compte);
		
		System.out.println(retour + " lignes ajoutees");

		//test de la m�thode getContrat
		//Compte a2 = compteDAO.getCompte("Responsable");
		//System.out.println(a2);

		// test de la méthode getListeContrat
		List<Compte> liste = compteDAO.getListeComtpe();
		// affichage des clients
		for (Compte ctr : liste) {
			System.out.println(ctr.toString());
		}

	}
}
