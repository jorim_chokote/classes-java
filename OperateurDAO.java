import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe d'acc�s aux donn�es contenues dans la table operateur_opr
 * 
 * @author Chokote & Leondaris
 * @version 1.0
 * */
public class OperateurDAO {

	/**
	 * Paramètres de connexion à la base de données oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "SYSTEM";  //exemple BDD1
	final static String PASS = "jorim1996";   //exemple BDD1
	double identifiant;

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public OperateurDAO() {
		// chargement du pilote de bases de données
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err
					.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * Permet d'ajouter un client dans la table client Le mode est auto-commit
	 * par défaut : chaque insertion est validée
	 * 
	 * @param client
	 *            le client à ajouter
	 * @return retourne le nombre de lignes ajoutées dans la table
	 */
	public int ajouter(Operateur operateur) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;

		// connexion à la base de données
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// préparation de l'instruction SQL, chaque ? représente une valeur
			// à communiquer dans l'insertion
			// les getters permettent de récupérer les valeurs des attributs
			// souhaités
			
			ps = con.prepareStatement("INSERT INTO operateur_opr (id_opr, nom_opr, pre_opr, tel_opr, opr_date_mes) VALUES (seq_opr.NEXTVAL, ?, ?, ?, ?)");
			ps.setString(1, operateur.getNom());
			ps.setString(2, operateur.getPrenom());
			ps.setLong(3, operateur.getNumTelephone());
			ps.setDate(4, operateur.getDateMes());
			


			// Ex�cution de la requ�te
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de retrouver un contrat � partir d'un client
	 * @param client
	 * @return
	 */
	public Operateur getOperateur(String nom, String prenom) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Operateur retour = null;

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM operateur_opr WHERE nom_opt LIKE '%?%' AND pre_opt LIKE '%?%'");
			ps.setString(1, nom);
			ps.setString(2, prenom);

			// on exécute la requête
			// rs contient un pointeur situé juste avant la première ligne
			// retournée
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			if (rs.next()){
				retour = new Operateur(
						rs.getString("nom_opr"),
						rs.getString("pre_opr"), rs.getLong("tel_opr"), rs.getDate("opr_date_mes"));
				identifiant = rs.getDouble("id_opr");
			}
						
			
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer tous les clients stockés dans la table client_clt
	 * 
	 * @return une ArrayList de Client
	 */
	public List<Operateur> getListeOperateur() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Operateur> retour = new ArrayList<Operateur>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM operateur_opr");

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next())
				retour.add(new Operateur(rs.getString("nom_opr"), rs.getString("pre_opr"), rs.getLong("tel_opr"), rs.getDate("opr_date_mes")));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	// main permettant de tester la classe
	public static void main(String[] args) throws SQLException {

		OperateurDAO operateurDAO = new OperateurDAO();
		// test de la m�thode ajouter
		Operateur o1 = new Operateur("Hasina", "Joely", 612345678L, Date.valueOf("2013-05-15"));
		
		int retour = operateurDAO.ajouter(o1);

		System.out.println(retour + " lignes ajoutées");

		//test de la m�thode getContrat
		//Contrat a2 = contratDAO.getContrat(c1);
		//System.out.println(a2);

		// test de la méthode getListeContrat
		List<Operateur> liste = operateurDAO.getListeOperateur();
		// affichage des utilisateur
		for (Operateur ctr : liste) {
			System.out.println(ctr.toString());
		}

	}
}
