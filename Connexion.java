import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe d'acc�s aux donn�es contenues dans la table operateur_opr
 * 
 * @author Chokote & Leondaris
 * @version 1.0
 * */
public class Connexion {

	/**
	 * Paramètres de connexion à la base de données oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "SYSTEM";  //exemple BDD1
	final static String PASS = "jorim1996";   //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public Connexion() {
		// chargement du pilote de bases de données
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err
					.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * Permet d'ajouter un client dans la table client Le mode est auto-commit
	 * par défaut : chaque insertion est validée
	 * 
	 * @param client
	 *            le client à ajouter
	 * @return retourne le nombre de lignes ajoutées dans la table
	 */
	 public String seConnecter(String login, String mdp) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String fonction = null;

		// connexion à la base de données
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// préparation de l'instruction SQL, chaque ? représente une valeur
			// à communiquer dans l'insertion
			// les getters permettent de récupérer les valeurs des attributs
			// souhaités
			
			ps = con.prepareStatement("SELECT fonction_cpt FROM compte_cpt INNER JOIN utilisateur_utl ON compte_cpt.id_cpt = utilisateur_utl.id_utl_cpt WHERE utilisateur_utl.log_utl = ? AND utilisateur_utl.mdp_utl = ?");
			ps.setString(1, login);
			ps.setString(2, mdp);
			
			// Ex�cution de la requ�te
			rs = ps.executeQuery();
			if (rs.next())
				fonction = rs.getString("fonction_cpt");
			
			

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return fonction;

	}


	// main permettant de tester la classe
	public static void main(String[] args) throws SQLException {

		Connexion connexion = new Connexion();
		String fonction = connexion.seConnecter("jorim.chokote", "chokote.jorim");
		System.out.println(fonction);
	}
}