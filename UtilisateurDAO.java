import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe d'acc�s aux donn�es contenues dans la table utilisateur_utl
 * 
 * @author Chokote & Leondaris
 * @version 1.0
 * */
public class UtilisateurDAO {

	/**
	 * Paramètres de connexion à la base de données oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "SYSTEM";  //exemple BDD1
	final static String PASS = "jorim1996";   //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public UtilisateurDAO() {
		// chargement du pilote de bases de données
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err
					.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * Permet d'ajouter un client dans la table client Le mode est auto-commit
	 * par défaut : chaque insertion est validée
	 * 
	 * @param client
	 *            le client à ajouter
	 * @return retourne le nombre de lignes ajoutées dans la table
	 */
	public int ajouter(Utilisateur utilisateur) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;

		// connexion à la base de données
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// préparation de l'instruction SQL, chaque ? représente une valeur
			// à communiquer dans l'insertion
			// les getters permettent de récupérer les valeurs des attributs
			// souhaités
			
			ps = con.prepareStatement("INSERT INTO utilisateur_utl (id_utl, nom_utl, pre_utl, tel_utl, log_utl, mdp_utl, id_utl_cpt) VALUES (?, ?, ?, ?, ?, ?, ?)");
			ps.setDouble (1, utilisateur.getIdentifiant());
			ps.setString(2, utilisateur.getNom());
			ps.setString(3, utilisateur.getPrenom());
			ps.setLong(4, utilisateur.getNumTelephone());
			ps.setString(5, utilisateur.getLogin());
			ps.setString(6, utilisateur.getMdp());
			ps.setDouble(7, utilisateur.getCompte().getIdentifiant());


			// Ex�cution de la requ�te
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de retrouver un contrat � partir d'un client
	 * @param client
	 * @return
	 */
	public Utilisateur getUtilisateur(String login) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Utilisateur retour = null;

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM utilisateur_utl INNER JOIN compte_cpt  ON utilisateur_utl.id_utl = compte_cpt.id_cpt WHERE log_utl = '?'");
			ps.setString(1, login);

			// on exécute la requête
			// rs contient un pointeur situé juste avant la première ligne
			// retournée
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			if (rs.next())
				retour = new Utilisateur(rs.getDouble("id_utl"),
						rs.getString("nom_utl"),
						rs.getString("pre_utl"), rs.getLong("tel_utl"), login, rs.getString("mdp_utl"), new Compte(rs.getDouble("id_cpt"), rs.getString("fonction_cpt")));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer tous les clients stockés dans la table client_clt
	 * 
	 * @return une ArrayList de Client
	 */
	public List<Utilisateur> getListeUtilisateur() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Utilisateur> retour = new ArrayList<Utilisateur>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM utilisateur_utl INNER JOIN compte_cpt  ON utilisateur_utl.id_utl_cpt = compte_cpt.id_cpt");

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next())
				retour.add(new Utilisateur(rs.getDouble("id_utl"), rs.getString("nom_utl"), rs.getString("pre_utl"), rs.getLong("tel_utl"), rs.getString("log_utl"), rs.getString("mdp_utl"), new Compte(rs.getDouble("id_cpt"), rs.getString("fonction_cpt"))));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	// main permettant de tester la classe
	public static void main(String[] args) throws SQLException {

		UtilisateurDAO utilisateurDAO = new UtilisateurDAO();
		// test de la m�thode ajouter
		Compte compte = new Compte(1, "Salarie");
		Utilisateur u1 = new Utilisateur(1, "Chokote", "Jorim", 612345678L, "jorim.chokote", "chokote.jorim", compte  );
		
		int retour = utilisateurDAO.ajouter(u1);

		System.out.println(retour + " lignes ajoutées");

		//test de la m�thode getContrat
		//Contrat a2 = contratDAO.getContrat(c1);
		//System.out.println(a2);

		// test de la méthode getListeContrat
		List<Utilisateur> liste = utilisateurDAO.getListeUtilisateur();
		// affichage des utilisateur
		for (Utilisateur ctr : liste) {
			System.out.println(ctr.toString());
		}

	}
}
