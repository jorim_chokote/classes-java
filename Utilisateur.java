/**
 * Class Utilisateur
 * @author Chokote & Leondaris
 *
 */
public class Utilisateur {
	/**
	 * Identifiant de l'utilisateur
	 */
	private double identifiant;
	
	/**
	 * Nom de l'utilisateur
	 */
	private String nom;
	
	/**
	 * �Pr�nom de l'utilisateur 
	 */
	private String prenom;
	
	/**
	 * Num�ro de t�l�phone de l'utilisateur
	 */
	private long numTelephone;
	
	/**
	 * Login de l'utilisateur
	 */
	private String login;
	
	/**
	 * Mot de passe de l'utilisateur
	 */
	private String mdp;
	
	/**
	 *Compte de l'utilisateur 
	 */
	private Compte compte;
	
	
	/**
	 * Constructeur de la classe
	 * @param identifiant
	 * @param nom
	 * @param prenom
	 * @param numTelephone
	 * @param login
	 * @param mdp
	 * @param compte
	 */
	public Utilisateur (double identifiant, String nom, String prenom, long numTelephone, String login, String mdp, Compte compte){
		this.identifiant = identifiant;
		this.nom = nom;
		this.prenom = prenom;
		this.numTelephone = numTelephone;
		this.login = login;
		this.mdp = mdp;
		this.compte = compte;
	}
	
	/**
	 * Getter de l'identifiant de l'utilisateur 
	 * @return
	 */
	public double getIdentifiant(){
		return identifiant;
	}
	
	/**
	 * Getter du nom de l'utilisateur
	 * @return
	 */
	public String getNom(){
		return nom;
	}
	
	/**
	 * Getter du pr�nom de l'utilisateur
	 * @return
	 */
	public String getPrenom(){
		return prenom;
	}
	
	/**
	 * Getter du num�ro de t�l�phone
	 * @return
	 */
	public long getNumTelephone(){
		return numTelephone;
	}
	
	/**
	 * Getter du login de l'utilisateur
	 * @return
	 */
	public String getLogin(){
		return login;
	}
	
	/**
	 * Getter du mot de passe de l'utilisateur
	 * @return
	 */
	public String getMdp(){
		return mdp;
	}
	
	/**
	 * Getter du compte de l'utilisateur
	 * @return
	 */
	public Compte getCompte(){
		return compte;
	}
	
	/**
	 * Setter de l'identifiant de l'utilisateur
	 * @param identifiant
	 */
	public void setIdentifiant(double identifiant){
		this.identifiant = identifiant;
	}
	
	/**
	 * Setter de nom de l'utilisateur
	 * @param nom
	 */
	public void setNom(String nom){
		this.nom = nom;
	}
	
	/**
	 * Setter du pr�nom de l'utilisateur
	 * @param prenom
	 */
	public void setPrenom(String prenom){
		this.prenom = prenom;
	}
	
	/**
	 * Setter du numero de telephone de l'utilisateur
	 * @param numTelephone
	 */
	public void setNumTelephone (long numTelephone){
		this.numTelephone = numTelephone;
	}
	
	/**
	 * Setter du login de l'utilisateur
	 * @param login
	 */
	public void setLogin (String login){
		this.login = login;
	}
	
	/**
	 * Setter du mdp de l'utilisateur
	 * @param mdp
	 */
	public void setMdp (String mdp){
		this.mdp = mdp;
	}
	
	/**
	 * Setter du compte de l'utilisateur
	 * @param compte
	 */
	public void setCompte (Compte compte){
		this.compte = compte;
	}
	
	public String toString(){
		return "Utilisateur : " +nom+ ", " +prenom+ ", " +0+numTelephone+ ", " +login+ ", " +mdp+ ", " +compte.getFonction(); 
	}
}
