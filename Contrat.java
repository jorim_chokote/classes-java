

/**
 * Classe contrat
 * @author Chokote & Leondaris
 * @version 1.0
 */
public class Contrat {
	/**
	 * identifiant du contrat
	 */
	private double identifiant;
	
	/**
	 * date de d�but du contrat
	 */
	private java.sql.Date dateDebut;
	
	/**
	 * date de fin du contrat
	 */
	private java.sql.Date dateFin;
	
	/**
	 * client ayant le contrat
	 */
	private Client client;

	
	
	/**
	 * Constructeur
	 * @param identifiant
	 * @param dateDebut
	 * @param dateFin
	 */
	public Contrat (double identifiant, java.sql.Date dateDebut, java.sql.Date dateFin, Client client){
		this.identifiant = identifiant;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.client = client;
	}
	
	/**
	 * Getter de l'identifiant
	 * @return
	 */
	public double getIdentifiant(){
		return identifiant;
	}
	
	/**
	 * Getter de la date de d�but du contrat
	 * @return
	 */
	public java.sql.Date getDateDebut(){
		return dateDebut;
	}
	
	/**
	 * Getter de la date de fin du contrat
	 * @return
	 */
	public java.sql.Date getDateFin(){
		return dateFin;
	}
	
	/**
	 * Getter du client
	 * @return
	 */
	public Client getClient(){
		return client;
	}
	
	/**
	 * Setter de l'identifiant
	 * @param identifiant
	 */
	public void setIdentifiant (double identifiant){
		this.identifiant = identifiant;
	}
	
	/**
	 * Setter de la date de debut
	 * @param dateDebut
	 */
	public void setDateDebut (java.sql.Date dateDebut){
		this.dateDebut = dateDebut;
	}
	
	/**
	 * Setter de la date de fin
	 * @param dateFin
	 */
	public void setDateFin (java.sql.Date dateFin){
		this.dateFin = dateFin;
	}
	
	/**
	 * Setter du client
	 * @param client
	 */
	public void setClient (Client client){
		this.client = client;
	}
	
	public String toString(){
		return "Contrat : " + client.getNomEntreprise() + ", date de debut du contrat : " + dateDebut + ", date de fin du contrat : " + dateFin;
	}
}
