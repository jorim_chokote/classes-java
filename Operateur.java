import java.sql.Date;

/**
 * Classe operateur
 * @author Chokote & Leondaris
 * @version 1.0
 */
public class Operateur {
	
	/**
	 * Identifiant de l'op�rateur
	 */
	private double identifiant;
	
	/**
	 * Nom de l'op�rateur
	 */
	private String nom;
	
	/**
	 * Pr�nom de l'op�rateur
	 */
	private String prenom;
	
	/**
	 * Num�ro de t�l�phone de l'op�rateur
	 */
	private long numTelephone;
	
	/**
	 * Date de mise en service de l'op�rateur
	 */
	private java.sql.Date dateMes;
	
	/**
	 * Constructeur de la classe
	 * @param identifiant
	 * @param nom
	 * @param prenom
	 * @param numTelephone
	 * @param dateMes
	 */
	public Operateur(String nom, String prenom, long numTelephone, java.sql.Date dateMes ){
		this.nom = nom;
		this.prenom = prenom;
		this.numTelephone = numTelephone;
		this.dateMes = dateMes;
	}

	
	/**
	 * Getter du nom de l'op
	 * @return
	 */
	public String getNom(){
		return nom;
	}
	
	/**
	 * Getter de prenom de l'op
	 * @return
	 */
	public String getPrenom(){
		return prenom;
	}
	
	/**
	 * Getter du num�ro de tel de l'op
	 * @return
	 */
	public long getNumTelephone(){
		return numTelephone;
	}
	
	/**
	 * Getter de la date d'embauche de l'operateur
	 * @return
	 */
	public java.sql.Date getDateMes(){
		return dateMes;
	}
	
	/**
	 * Setter du nom de l'op
	 * @param nom
	 */
	public void setNom(String nom){
		this.nom = nom;
	}
	
	
	/**
	 * Setter du prenom de l'op
	 * @param prenom
	 */
	public void setPrenom(String prenom){
		this.prenom = prenom;
	}
	
	/**
	 * Setter du num�ro de t�l�phone de l'op
	 * @param numTelephone
	 */
	public void setNumTelephone(long numTelephone){
		this.numTelephone = numTelephone;
	}
	
	/**
	 * Setter de la date d'embauche de l'op
	 * @param dateMes
	 */
	public void setDateMes(java.sql.Date dateMes){
		this.dateMes = dateMes;
	}
	
	
	public String toString(){
		return "Operateur : " +nom+ ", " +prenom+ ", " +0+numTelephone+ ", " +dateMes;
	}
}
